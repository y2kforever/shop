<?php

namespace backend\controllers\auth;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use shop\forms\auth\LoginForm;
use shop\services\auth\AuthService;

/**
 * Site controller
 */
class AuthController extends Controller
{

    public $service;

    public function __construct(
        $id,
        $module,
        AuthService $service,
        $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';
        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $user = $this->service->auth($form);
            Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $form,
        ]);

    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
