<?php

namespace frontend\controllers\auth;

use Yii;
use yii\authclient\{
    AuthAction, ClientInterface
};
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use shop\services\auth\NetworkService;

/**
 * Network controller
 */
class NetworkController extends Controller
{
    public $service;

    public function __construct(
        $id,
        $module,
        NetworkService $service,
        $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }


    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'onAuthSuccess']
            ]
        ];
    }

    public function onAuthSuccess(ClientInterface $client): void
    {
        $network = $client->getId();
        $attributes = $client->getUserAttributes();
        $identity = ArrayHelper::getValue($attributes, 'id');

        try {
            $user = $this->service->auth($network, $identity);
            Yii::$app->user->login($user, 3600 * 24 * 30);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

    }
}
