<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 04.06.2018
 * Time: 1:04
 */

namespace shop\tests\unit\entities\shop\tag;

use Codeception\Test\Unit;

class CreateTest extends Unit
{

    public function testSuccess()
    {

        $tag = Tag::create(
            $name = 'Name',
            $slag = 'slug'
        );

        $this->assertEquals($name, $tag->name);
        $this->assertEquals($slag, $tag->slug);

    }

}