<?php

namespace shop\helpers;

use yii\helpers\{
    Html, ArrayHelper
};
use shop\entities\user\User;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 03.06.2018
 * Time: 16:05
 */
class UserHelper
{
    public static function statusList(): array
    {
        return [
            User::STATUS_WAIT => 'Wait',
            User::STATUS_ACTIVE => 'Active',
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case User::STATUS_WAIT:
                $class = 'label label-default';
                break;
            case User::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

}