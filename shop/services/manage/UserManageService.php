<?php

namespace shop\services\manage;

use shop\forms\manage\user\{
    UserEditForm, UserCreateForm
};
use shop\entities\user\User;
use shop\repositories\UserRepository;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 03.06.2018
 * Time: 23:33
 */
class UserManageService
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(UserCreateForm $form): User
    {
        $user = User::create(
            $form->username,
            $form->email,
            $form->password
        );
        $this->repository->save($user);
        return $user;
    }

    public function edit($id, UserEditForm $form): void
    {
        $user = $this->repository->get($id);
        $user->edit(
            $form->username,
            $form->email
        );

        $this->repository->save($user);
    }
}