<?php

namespace shop\services\contact;

use shop\forms\ContactForm;
use yii\mail\MailerInterface;
use shop\repositories\UserRepository;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 17.05.2018
 * Time: 1:28
 */
class ContactService
{

    private $user;
    private $mailer;
    private $adminEmail;

    public function __construct($adminEmail, MailerInterface $mailer, UserRepository $user)
    {
        $this->user = $user;
        $this->mailer = $mailer;
        $this->adminEmail = $adminEmail;
    }

    public function send(ContactForm $form): void
    {
        $sent = $this->mailer->compose()
            ->setTo($this->adminEmail)
            ->setSubject($form->subject)
            ->setTextBody($form->body)
            ->send();

        if (!$sent) {
            throw new \RuntimeException('Sending error.');
        }
    }

}