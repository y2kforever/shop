<?php

namespace shop\services\auth;

use shop\entities\user\User;
use yii\mail\MailerInterface;
use shop\forms\auth\SignupForm;
use shop\repositories\UserRepository;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 17.05.2018
 * Time: 0:23
 */
class SignupService
{
    private $user;
    private $mailer;

    public function __construct(UserRepository $user, MailerInterface $mailer)
    {
        $this->user = $user;
        $this->mailer = $mailer;
    }

    public function signup(SignupForm $form): void
    {
        $user = User::requestSignup(
            $form->username,
            $form->email,
            $form->password
        );

        $this->user->save($user);

        $sent = $this->mailer->compose(
            ['html' => 'auth/signup/confirm-html', 'text' => 'auth/signup/confirm-text'],
            ['user' => $user]
        )
            ->setTo($form->email)
            ->setSubject('Signup confirm for ' . \Yii::$app->name)
            ->send();

        if (!$sent) {
            throw new \RuntimeException('Email sending error.');
        }
    }

    public function confirm($token): void
    {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token.');
        }

        $user = $this->user->getByEmailConfirmToken($token);

        if (!$user) {
            throw new \DomainException('User is not found.');
        }

        $user->confirmSignup();
        $this->user->save($user);
    }

}