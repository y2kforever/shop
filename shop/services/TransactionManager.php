<?php

namespace shop\services;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 06.06.2018
 * Time: 14:22
 */
class TransactionManager
{

    public function wrap(callable $function): void
    {
        \Yii::$app->db->transaction($function);
    }

}