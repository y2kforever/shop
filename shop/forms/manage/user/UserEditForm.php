<?php

namespace shop\forms\manage\user;

use yii\base\Model;
use shop\entities\user\User;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 03.06.2018
 * Time: 23:48
 */
class UserEditForm extends Model
{
    public $username;
    public $email;

    public $_user;

    public function __construct(User $user, $config = [])
    {
        $this->username = $user->username;
        $this->email = $user->email;
        $this->_user = $user;
        parent::__construct($config);
    }


    public function rules(): array
    {
        return [
            [['username', 'email'], 'required'],
            ['email', 'email'],
            [['email'], 'string', 'max' => 255],
            [['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->_user->id]],
        ];
    }

    public function rolesList(): array
    {
        return ArrayHelper::map(\Yii::$app->authManager->getRoles(), 'name', 'description');
    }
}