<?php

namespace shop\forms\manage\user;

use yii\base\Model;
use shop\entities\user\User;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 03.06.2018
 * Time: 23:26
 */
class UserCreateForm extends Model
{
    public $username;
    public $email;
    public $password;

    public function rules(): array
    {
        return [
            [['username', 'email'], 'required'],
            ['email', 'email'],
            [['username', 'email'], 'string', 'max' => 255],
            [['username', 'email',], 'unique', 'targetClass' => User::class],
            ['password', 'string', 'min' => 6],
        ];
    }

}