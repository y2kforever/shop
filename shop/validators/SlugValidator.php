<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 05.06.2018
 * Time: 23:54
 */

namespace shop\validators;

class SlugValidator extends \yii\validators\RegularExpressionValidator
{

    public $pattern = '#^[a-z0-9_-]*$#s';
    public $message = 'Only [a-z0-9_-] symbols are allowed.';

}