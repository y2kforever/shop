<?php

namespace shop\repositories\shop;

use shop\entities\shop\Tag;
use shop\repositories\NotFoundException;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 17.05.2018
 * Time: 2:47
 */
class TagRepository
{
    public function get(int $id): Tag
    {
        if (!$tag = Tag::findOne($id)) {
            throw new NotFoundException('Tag is not found.');
        }
        return $tag;
    }

    public function findByName($name): ?Tag
    {
        return Tag::findOne(['name' => $name]);
    }

    public function save(Tag $tag): void
    {
        if (!$tag->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Tag $tag): void
    {
        if (!$tag->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }


}