<?php

namespace shop\repositories;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 23.05.2018
 * Time: 3:56
 */
class NotFoundException extends \DomainException
{

}