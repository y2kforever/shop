<?php

namespace shop\entities\shop\queries;

use yii\db\ActiveQuery;
use paulzi\nestedsets\NestedSetsQueryTrait;

class CategoryQuery extends ActiveQuery
{
    use NestedSetsQueryTrait;
}