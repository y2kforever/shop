<?php

namespace shop\entities\shop\product\queries;

use yii\db\ActiveQuery;
use shop\entities\shop\product\Product;

class ProductQuery extends ActiveQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'status' => Product::STATUS_ACTIVE,
        ]);
    }
}