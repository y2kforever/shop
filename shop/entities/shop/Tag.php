<?php

namespace shop\entities\shop;

use yii\db\ActiveRecord;

/**
 * Created by PhpStorm.
 * @author Adikhanov Abdugani <y2k.gostop@gmail.com>
 * Date: 04.06.2018
 * Time: 01:30
 * @property integer $id
 * @property string $name
 * @property string $slug
 */
class Tag extends ActiveRecord
{
    public static function create($name, $slug): self
    {
        $tag = new static();
        $tag->name = $name;
        $tag->slug = $slug;
        return $tag;
    }

    public function edit($name, $slug): void
    {
        $this->name = $name;
        $this->slug = $slug;
    }

    public static function tableName()
    {
        return '{{%shop_tags}}';
    }
}