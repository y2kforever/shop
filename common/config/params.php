<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'staticHostInfo' => 'http://static...',
    'staticPath' => dirname(__DIR__) . '/../static',
];
