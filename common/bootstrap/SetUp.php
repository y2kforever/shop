<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 17.05.2018
 * Time: 1:20
 */

namespace common\bootstrap;


use yii\mail\MailerInterface;
use yii\base\BootstrapInterface;
use shop\services\contact\ContactService;
use shop\services\auth\PasswordResetService;

class SetUp implements BootstrapInterface
{

    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->setSingleton(PasswordResetService::class);

        $container->setSingleton(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });
        $container->setSingleton(ContactService::class, [], [
            $app->params['adminEmail']
        ]);

    }

}